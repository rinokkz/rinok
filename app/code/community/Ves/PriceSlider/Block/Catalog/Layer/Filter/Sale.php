<?php
/* app/code/community/Inchoo/Sale/Block/Catalog/Layer/Filter/Sale.php */
 
class Ves_PriceSlider_Block_Catalog_Layer_Filter_Sale
extends Mage_Catalog_Block_Layer_Filter_Abstract
{
 
    public function __construct()
    {
        parent::__construct();
        $this->_filterModelName = 'ves_priceslider/catalog_layer_filter_sale';
    }
}