<?php
/* app/code/community/Inchoo/Sale/Block/Catalog/Layer/View.php */
 
class Ves_PriceSlider_Block_Catalog_Layer_View 
extends Mage_Catalog_Block_Layer_View
{
    const SALE_FILTER_POSITION = 2;
    const STOCK_FILTER_POSITION = 3;
    
    protected $config = array();
    /**
     * State block name
     *
     * @var string
     */
    protected $_saleBlockName;

    /**
     * State block name
     *
     * @var string
     */
    protected $_stockBlockName;
 
    /**
     * Initialize blocks names
     */
    protected function _initBlocks()
    {
        parent::_initBlocks();
        
        $this->config = Mage::getStoreConfig('ves_priceslider');

        if(!$this->config['priceslider_setting']['show']) {
            return;
        }

        $this->_saleBlockName = 'ves_priceslider/catalog_layer_filter_sale';
        $this->_stockBlockName = 'ves_priceslider/catalog_layer_filter_stock';
    }
 
    /**
     * Prepare child blocks
     *
     * @return Mage_Catalog_Block_Layer_View
     */
    protected function _prepareLayout()
    {
        //If enable sale filter
        if(isset($this->config['ves_layerfilter_conf']['show_sale_filter']) && $this->config['ves_layerfilter_conf']['show_sale_filter']) {
            
            $saleBlock = $this->getLayout()->createBlock($this->_saleBlockName)
                ->setLayer($this->getLayer())
                ->init();
 
            $this->setChild('sale_filter', $saleBlock);

        }
        //If enable stock filter
        if(isset($this->config['ves_layerfilter_conf']['show_stock_filter']) && $this->config['ves_layerfilter_conf']['show_stock_filter']) {
            $stockBlock = $this->getLayout()->createBlock($this->_stockBlockName)
                    ->setLayer($this->getLayer())
                    ->init();
     
            $this->setChild('stock_filter', $stockBlock);
        }
 
        return parent::_prepareLayout();
    }
 
    /**
     * Get all layer filters
     *
     * @return array
     */
    public function getFilters()
    {
        $filters = parent::getFilters();
        //If enable sale filter
        if(isset($this->config['ves_layerfilter_conf']['show_sale_filter']) && $this->config['ves_layerfilter_conf']['show_sale_filter']) {
            if (($saleFilter = $this->_getSaleFilter())) {
                // Insert sale filter to the self::SALE_FILTER_POSITION position
                $sale_filters = array_merge(
                    array_slice(
                        $filters,
                        0,
                        self::SALE_FILTER_POSITION - 1
                    ),
                    array($saleFilter),
                    array_slice(
                        $filters,
                        self::SALE_FILTER_POSITION - 1,
                        count($filters) - 1
                    )
                );
                $filters = $sale_filters;
            }
        }
        //If enable stock filter
        if(isset($this->config['ves_layerfilter_conf']['show_stock_filter']) && $this->config['ves_layerfilter_conf']['show_stock_filter']) {
            if (($stockFilter = $this->_getStockFilter())) {
                // Insert sale filter to the self::SALE_FILTER_POSITION position
                $stock_filters = array_merge(
                    array_slice(
                        $filters,
                        0,
                        self::STOCK_FILTER_POSITION - 1
                    ),
                    array($stockFilter),
                    array_slice(
                        $filters,
                        self::STOCK_FILTER_POSITION - 1,
                        count($filters) - 1
                    )
                );
                $filters = $stock_filters;
            }
        }
 
        return $filters;
    }
 
    /**
     * Get sale filter block
     *
     * @return Mage_Catalog_Block_Layer_Filter_Sale
     */
    protected function _getSaleFilter()
    {
        return $this->getChild('sale_filter');
    }

    /**
     * Get sale filter block
     *
     * @return Mage_Catalog_Block_Layer_Filter_Sale
     */
    protected function _getStockFilter()
    {
        return $this->getChild('stock_filter');
    }
 
}