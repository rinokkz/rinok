<?php
/* app/code/community/Ves/PriceSlider/Model/Catalog/Layer/Filter/Stock.php */
 
class Ves_PriceSlider_Model_Catalog_Layer_Filter_Stock
extends Mage_Catalog_Model_Layer_Filter_Abstract
{
 
    const FILTER_IN_STOCK = 1;
    const FILTER_OUT_STOCK = 2;
 
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_requestVar = 'stock';
    }
 
    /**
     * Apply stock filter to layer
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Mage_Core_Block_Abstract $filterBlock
     * @return  Mage_Catalog_Model_Layer_Filter_Sale
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = (int) $request->getParam($this->getRequestVar());
        if (!$filter || Mage::registry('ves_stock_filter')) {
            return $this;
        }
        $collection = $this->getLayer()->getProductCollection();
        if(!Mage::registry('ves_stock_joined')) {
            $collection->joinField(
                'qty',
                'cataloginventory/stock_item',
                'is_in_stock',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );

            Mage::register('ves_stock_joined', true);
        }
        $select = $collection->getSelect();
        /* @var $select Zend_Db_Select */
        
        if ($filter == self::FILTER_IN_STOCK) {
            $select->where('at_qty.is_in_stock = 1');
            $stateLabel = Mage::helper('ves_priceslider')->__('In Stock');

        } elseif($filter == self::FILTER_OUT_STOCK) {
            $select->where('at_qty.is_in_stock = 0');
            $stateLabel = Mage::helper('ves_priceslider')->__('Out of stock');
        }
        $state = $this->_createItem(
            $stateLabel, $filter
        )->setVar($this->_requestVar);
        /* @var $state Mage_Catalog_Model_Layer_Filter_Item */
 
        $this->getLayer()->getState()->addFilter($state);
 
        Mage::register('ves_stock_filter', true);
 
        return $this;
    }
 
    /**
     * Get filter name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('ves_priceslider')->__('Stock');
    }
 
    /**
     * Get data array for building sale filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {

        $data = array();
        $status = $this->_getCount();
        
        if($status['yes'] !== null) {
            $data[] = array(
                'label' => Mage::helper('ves_priceslider')->__('In Stock'),
                'value' => self::FILTER_IN_STOCK,
                'count' => $status['yes'],
            );
        }
        
        if($status['no'] !== null) {
            $data[] = array(
                'label' => Mage::helper('ves_priceslider')->__('Out of stock'),
                'value' => self::FILTER_OUT_STOCK,
                'count' => $status['no'],
            );
        }
        return $data;
    }
 
    protected function _getCount()
    {
        // Clone the select
        $select = clone $this->getLayer()->getProductCollection()->getSelect();
        /* @var $select Zend_Db_Select */
 
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        //$select->reset(Zend_Db_Select::WHERE);
 
        // Count the on sale and not on sale
        $sql = 'SELECT IF(qty = 0, "no", "yes") as in_stock, COUNT(*) as count from ('
                .$select->__toString().') AS q GROUP BY in_stock';
 
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        /* @var $connection Zend_Db_Adapter_Abstract */
 
        return $connection->fetchPairs($sql);
    }
 
}