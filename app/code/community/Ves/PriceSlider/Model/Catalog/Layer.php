<?php

/**
 * Catalog view layer model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Venustheme <venustheme@gmail.com>
 */
class Ves_PriceSlider_Model_Catalog_Layer extends Mage_Catalog_Model_Layer
{
	const FILTER_ON_SALE = 1;
    const FILTER_NOT_ON_SALE = 2;
    const FILTER_IN_STOCK = 1;
    const FILTER_OUT_STOCK = 2;
	/*
	* Add Filter in product Collection for new price
	*
	* @return object
	*/
    public function getProductCollection()
    {
        if (isset($this->_productCollections[$this->getCurrentCategory()->getId()])) {
            $collection = $this->_productCollections[$this->getCurrentCategory()->getId()];
        } else {
            $collection = $this->getCurrentCategory()->getProductCollection();
            $this->prepareProductCollection($collection);
            $this->_productCollections[$this->getCurrentCategory()->getId()] = $collection;
        }
		
		$config = Mage::getStoreConfig('ves_priceslider');
		if(isset($config['ves_layerfilter_conf']['show_stock_filter']) && $config['ves_layerfilter_conf']['show_stock_filter']) {
			if(!Mage::registry('ves_stock_joined')) {
				$collection->joinField(
                    'qty',
                    'cataloginventory/stock_item',
                    'is_in_stock',
                    'product_id=entity_id',
                    '{{table}}.stock_id=1',
                    'left'
                );

                Mage::register('ves_stock_joined', true);
			}
		}

		$this->currentRate = Mage::app()->getStore()->getCurrentCurrencyRate();;
		$max=$this->getMaxPriceFilter();
		$min=$this->getMinPriceFilter();
		
		if($min && $max){
			$collection->getSelect()->where(' final_price >= "'.$min.'" AND final_price <= "'.$max.'" ');
		}


        return $collection;
    }
	
	
	/*
	* convert Price as per currency
	*
	* @return currency
	*/
	public function getMaxPriceFilter(){
		return isset($_GET['max'])?round($_GET['max']/$this->currentRate):0;
	}
	
	
	/*
	* Convert Min Price to current currency
	*
	* @return currency
	*/
	public function getMinPriceFilter(){
		return isset($_GET['min'])?round($_GET['min']/$this->currentRate):0;
	}

	/*
	* Convert Min Price to current currency
	*
	* @return currency
	*/
	public function getSaleFilter(){
		return isset($_GET['sale'])?$_GET['sale']:0;
	}
    
    /*
	* Convert Min Price to current currency
	*
	* @return currency
	*/
	public function getStockFilter(){
		return isset($_GET['stock'])?$_GET['stock']:0;
	}
    
	
}
