<?php
/**
 * Venustheme
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Venustheme EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.venustheme.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.venustheme.com/ for more information
 *
 * @category   Ves
 * @package    Ves_Blog
 * @copyright  Copyright (c) 2014 Venustheme (http://www.venustheme.com/)
 * @license    http://www.venustheme.com/LICENSE-1.0.html
 */

/**
 * Ves Blog Extension
 *
 * @category   Ves
 * @package    Ves_Blog
 * @author     Venustheme Dev Team <venustheme@gmail.com>
 */
class Ves_Blog_Block_Blog_Post_Relatedproduct extends Mage_Catalog_Block_Product_Abstract
{

	public function __construct($attributes = array())
	{
		parent::__construct( $attributes );
		if(!$this->getGeneralConfig("show")) {
			return;
		}
		if(!$this->getPostConfig("post_showrelatedproduct")) {
			return;
		}

		if(isset($attributes['template']) && $attributes['template']) {
			$this->setTemplate($attributes['template']);
		} elseif($this->hasData("template")) {
			$this->setTemplate($this->getData('template'));
		} else {
			$template = 'ves/blog/post/relatedproduct.phtml';
			$this->setTemplate( $template );
		}
	}

	public function getPostConfig( $key ){
		return Mage::getStoreConfig('ves_blog/post_setting/'.$key);
	}

	public function getGeneralConfig( $key ){
		return Mage::getStoreConfig('ves_blog/general_setting/'.$key);
	}

	public function getProductConfig( $key ){
		return Mage::getStoreConfig('ves_blog/product_setting/'.$key);
	}

	public function getRelatedProducts(){
		$post_data = Mage::registry("current_post");
		$_products = array();
		if($post_data) {
			$storeId    = Mage::app()->getStore()->getId();
			$limit = $this->getPostConfig("post_showrelatedproductlimit");
			$products = $this->getCollectionPro()
								->joinField('position',
							                'ves_blog/post_product',
							                'position',
							                'product_id=entity_id',
							                'post_id='.(int) $post_data->getId(),
							                'left')
						        ->addAttributeToSort('position', 'asc')
							    ->addMinimalPrice()
							    ->addFinalPrice()
							    ->addStoreFilter($storeId)
							    ->addUrlRewrite()
							    ->addTaxPercents()
							    ->groupByAttribute('entity_id');

			$products->getSelect()->where(' at_position.post_id = '.(int) $post_data->getId());

	        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
	        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);

	        $products->setPageSize( $limit )->setCurPage( 1 );

	        $this->setProductCollection($products);
			$this->_addProductAttributesAndPrices($products);
			if (($_list = $this->getProductCollection ()) && $_list->getSize ()) {
				$_products = $products;
			}
		}

		return $_products;
	}

	public function getCollectionPro($model_type = 'catalog/product_collection')
    {
      $storeId = Mage::app()->getStore()->getId();
      $productFlatTable = Mage::getResourceSingleton('catalog/product_flat')->getFlatTableName($storeId);
      $attributesToSelect = array('name','entity_id','price', 'small_image','short_description');
      try{
		        /**
		        * init resource singleton collection
		        */
		        $products = Mage::getResourceModel($model_type);//Mage::getResourceSingleton('reports/product_collection');
		        if(Mage::helper('catalog/product_flat')->isEnabled()){
		          $products->joinTable(array('flat_table'=>$productFlatTable),'entity_id=entity_id', $attributesToSelect);
		        }else{
		          $products->addAttributeToSelect($attributesToSelect);
		        }
		        $products->addStoreFilter($storeId);
        return $products;
      }catch (Exception $e){
        Mage::logException($e->getMessage());
      }
    }
}