<?php
/*------------------------------------------------------------------------
 # Ves Ajax Scroll Module 
 # ------------------------------------------------------------------------
 # author:    Venustheme.Com
 # copyright: Copyright (C) 2012 http://www.Venustheme.com. All Rights Reserved.
 # @license: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Websites: http://www.Venustheme.com
 # Technical Support:  http://www.Venustheme.com/
-------------------------------------------------------------------------*/
class Ves_Blog_Block_Ajaxscroll extends Ves_Blog_Block_List
{

	
	public function isEnabled()
	{
		if($this->getConfig("enable_infinite_load", "list_setting")) {
			return true;
		} 
		return false;
	}

	/**
	 * @return bool|false
	 */
	public function getLoaderImage()
	{
		$url = Mage::helper('ves_ajaxscroll')->getConfigData('design/loading_img');
		if(!empty($url)) {
			$url = strpos($url, 'http') === 0 ? $url : $this->getSkinUrl($url);
		}
		return empty($url) ? false : $url;
	}

	public function getProductListMode()
	{
		// user mode
		if ($currentMode = $this->getRequest()->getParam('mode')) {
			switch($currentMode){
				case 'grid':
					$productListMode = 'grid';
					break;
				case 'list':
					$productListMode = 'list';
					break;
				default:
					$productListMode = 'grid';
			}
		}
		else {
			$defaultMode = Mage::getStoreConfig('catalog/frontend/list_mode');
			switch($defaultMode){
				case 'grid-list':
					$productListMode = 'grid';
					break;
				case 'list-grid':
					$productListMode = 'list';
					break;
				default:
					$productListMode = 'grid';
			}
		}

		return $productListMode;
	}

	public function getAjaxLoader() {
		return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."wysiwyg/ajax_loader.gif";
	}

}
