<?php
/**
 * Venustheme
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Venustheme EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.venustheme.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.venustheme.com/ for more information
 *
 * @category   Ves
 * @package    Ves_Blog
 * @copyright  Copyright (c) 2014 Venustheme (http://www.venustheme.com/)
 * @license    http://www.venustheme.com/LICENSE-1.0.html
 */

/**
 * Ves Blog Extension
 *
 * @category   Ves
 * @package    Ves_Blog
 * @author     Venustheme Dev Team <venustheme@gmail.com>
 */
class Ves_Blog_Block_Product_Relatedpost extends Ves_Blog_Block_List
{

	/**
	 * Contructor
	 */
	public function __construct($attributes = array())
	{
		parent::__construct( $attributes );

		if(!$this->getProductConfig("show_related_blog")) {
			return ;
		}

		$my_template = "";
		if(isset($attributes['template']) && $attributes['template']) {
			$my_template = $attributes['template'];
		}elseif($this->hasData("template")) {
			$my_template = $this->getData("template");
		}else {
			$my_template = "ves/blog/block/related_posts.phtml";
		}

		$this->setTemplate($my_template);
	}

	public function getBlogPosts(){
		$posts = array();
		if($product = Mage::registry('current_product')) {
			$sort_type = $this->getProductConfig("related_post_sort");
			$sort_type = $sort_type?$sort_type:'hits';
			$sort_order = $this->getProductConfig("related_post_order");
			$sort_order = $sort_order?$sort_order:'DESC';
			$limit = $this->getProductConfig("related_blog_limit");
			$limit = $limit?(int)$limit:10;

			$collection = Mage::getModel( 'ves_blog/post' )
						->getCollection()
						->addEnableFilter(1)
						->joinTable(
								array('position','ves_blog/post_product'),
								'main_table.post_id=position.post_id', 
								array(),
								null,
								null,
								'left')
						->setOrder( $sort_type, $sort_order );

			$collection->getSelect()->where(' position.product_id = '.(int) $product->getId());
						
			$collection->setPageSize( $limit );

			if ($collection->getSize ()) {
				$posts = $collection;
			}
		}
		return $posts;
	}
	public function getCountingComment( $post_id = 0){
		$comment = Mage::getModel('ves_blog/comment')->getCollection()
		->addEnableFilter( 1  )
		->addStoreFilter(Mage::app()->getStore()->getId())
		->addPostFilter( $post_id  );
		return count($comment);
	}
	public function getProductConfig( $key ){
		return Mage::getStoreConfig('ves_blog/product_setting/'.$key);
	}
}