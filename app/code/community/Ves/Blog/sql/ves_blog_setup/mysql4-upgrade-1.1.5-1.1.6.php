<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();
$installer->run("
	-- DROP TABLE IF EXISTS `{$this->getTable('ves_blog/post_product')}`;
	CREATE TABLE `{$this->getTable('ves_blog/post_product')}` (
	  `post_id` int(11) unsigned NOT NULL DEFAULT '0',
	  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
	  `position` int(11) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`post_id`,`product_id`),
	  CONSTRAINT `FK_BLOG_POST_PRODUCT_POST` FOREIGN KEY (`post_id`) REFERENCES `{$this->getTable('ves_blog/post')}` (`post_id`) ON UPDATE CASCADE ON DELETE CASCADE,
	  CONSTRAINT `FK_BLOG_POST_PRODUCT_PRODUCT` FOREIGN KEY (`product_id`) REFERENCES `{$this->getTable('catalog_product_entity')}` (`entity_id`) ON UPDATE CASCADE ON DELETE CASCADE
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Posts Products';
");

